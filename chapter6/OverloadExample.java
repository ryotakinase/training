package chapter6;

public class OverloadExample {
	public static void main(String[] args) {
		Point p1 = new Point();
		System.out.println("p1の座標は（" + p1.x + ", " + p1.y + "）");

		p1.set(10,0);
		System.out.println("p1の座標は（" + p1.x + ", " + p1.y + "）");

		Point p2 = new Point();
		p2.set(p1);
		System.out.println("p1の座標は（" + p1.x + ", " + p1.y + "）");
	}

}
