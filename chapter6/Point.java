package chapter6;

public class Point {
	int x;
	int y;
	
	Point(){
		System.out.println("引数のないコンストラクタが実行されました");
		this.x = 0;
		this.y = 0;
	}
	
	Point(Point p){
		System.out.println("引数が一つのコンストラクタが実行されました");
		this.x = p.x;
		this.y = p.y;
	}
	
	Point(int x, int y){
		System.out.println("引数が二つのコンストラクタ実行されました");
		this.x = x;
		this.y = y;
	}

	void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	void set(Point p) {
		this.x = p.x;
		this.y = p.y;
	}

}
