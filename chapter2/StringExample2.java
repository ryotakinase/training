package chapter2;

public class StringExample2 {
	public static void main(String[] args) {
		String message1 = "こんにちは";
		String message2 = "今日は良い天気ですね。";
		String message3 = message1 + message2;
		System.out.println(message3);
	}

}
