package chapter3;

public class TimeTable {
	public static void main(String[] args) {
		for(int i = 1; i <= 9; i++) {
			for(int j = 1; j <= 9; j++) {
				int value = i * j;
				System.out.print(j + "x" + i + "=" + value);
				System.out.print("  ");
			}
			System.out.println("");
		}
	}

}
