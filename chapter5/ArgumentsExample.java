package chapter5;

//class Point {
////	int x;
////	int y;
//
//	void printPosition() {
//		System.out.println("座標値は（" + this.x + ", " + this.y + "）です");
//	}
//
//	void multiply(int n) {
//		this.x *= n;
//		this.y *= n;
//	}
//
//	void add(int dx, int dy) {
//		this.x += dx;
//		this.y += dy;
//	}
//}

public class ArgumentsExample {
	public static void main(String[] args) {
		Point p = new Point();
		p.x = 5;
		p.y = 10;
		p.printPosition();

		p.multiply(2);
		p.printPosition();

		p.add(5, 3);
		p.printPosition();
	}

}
