package chapter5;

public class Point {

		int x;
		int y;

		void printPosition() {
			System.out.println("座標値は（" + this.x + ", " + this.y + "）です");
		}

		void multiply(int n) {
			this.x *= n;
			this.y *= n;
		}

		void add(int dx, int dy) {
			this.x += dx;
			this.y += dy;
		}


		int getXY() {
			return this.x * this.y;
		}

		boolean isSamePosition(Point p) {
			if(this.x == p.x && this.y == p.y) {
				return true;
			} else {
				return false;
			}
		}

		Point(int x, int y){
			System.out.println("Pointのコンストラクタが呼び出されました");
			this.x = x;
			this.y = y;
		}

		Point(){}


}
