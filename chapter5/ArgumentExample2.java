package chapter5;

public class ArgumentExample2 {
	public static void main(String[] args) {
		Point p1 = new Point();
		p1.x = 2;
		p1.y = 4;

		Point p2 = new Point();
		p2.x = 4;
		p2.y = 8;

		if(p1.isSamePosition(p2)) {
			System.out.println("p1とp2は同じ座標です");
		} else {
			System.out.println("p1とp2は異なる座標です");
		}

		p1.multiply(2);

		if(p1.isSamePosition(p2) == true) {
			System.out.println("p1とp2は同じ座標です");
		} else {
			System.out.println("p1とp2は異なる座標です");
		}

	}



}
