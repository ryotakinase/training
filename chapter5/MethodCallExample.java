package chapter5;

//class Point {
//	int x;
//	int y;
//
//	void printPosition() {
//		System.out.println("座標値は（" + this.x + ", " + this.y + "）です");
//	}
//}

public class MethodCallExample {
	public static void main(String[] args) {
		Point p1 = new Point();
		p1.x = 10;
		p1.y = 5;

		Point p2 = new Point();
		p2.x = 5;
		p2.y = 2;

		System.out.println("p1のメソッドの呼び出し");
		p1.printPosition();

		System.out.println("p2のメソッドの呼び出し");
		p2.printPosition();
	}

}
