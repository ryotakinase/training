package chapter4;

public class PointArrayExample {
	public static void main(String[] args) {
		Point[] points = new Point[5];
		for(int i = 0; i < 5; i++) {
			points[i] = new Point();
			points[i].x = i;
			points[i].y = i * i;
		}
		for(int i = 0; i < 5; i++) {
			System.out.println("points[" + i + "]の座標は（"
					+ points[i].x + "," + points[i].y + "）");
		}
	}

}
