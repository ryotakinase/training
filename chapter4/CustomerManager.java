package chapter4;

class CustomerCard {
	static int nextId = 1001;
	int id;
	String name;
	String address;
	double shoeSize;

	CustomerCard(String name, String address, double shoeSize){
		this.id = CustomerCard.nextId;
		CustomerCard.nextId++;

		this.name = name;
		this.address = address;
		this.shoeSize = shoeSize;
	}

	CustomerCard(String name){
		this(name, "", 0.0);
	}

	void printInfo() {
		System.out.println("ID:" + this.id);
		System.out.println("氏名:" + this.name);
		System.out.println("住所:" + this.address);
		System.out.println("靴のサイズ:" + this.shoeSize);
	}
}

public class CustomerManager {
	public static void main(String[] args) {
		CustomerCard[] cards = new CustomerCard[100];
		cards[0] = new CustomerCard("山田太郎", "東京都", 26.5);
		cards[1] = new CustomerCard("佐藤華子", "神奈川県", 24.5);
		cards[2] = new CustomerCard("鈴木健二", "茨城県", 26.0);

		for(int i = 0; i < 100; i++) {
			if(cards[i] == null) {
				break;
			}
			System.out.println(i + "番目の顧客カードに記載の情報");
			cards[i].printInfo();
			System.out.println("=================================");


		}
	}

}
